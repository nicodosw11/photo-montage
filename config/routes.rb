Rails.application.routes.draw do
  # get "/products/montage" => "products#montage", as: "products_montage"
  root to: 'products#index'
  resources :products do
    collection { post :upload }
  end
  # Sidekiq Web UI
  require "sidekiq/web"
  mount Sidekiq::Web => '/sidekiq'
end
