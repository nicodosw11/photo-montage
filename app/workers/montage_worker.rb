require 'open-uri'
class MontageWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(start_time, end_time)
    puts "sidekiq worker generating a montage from #{start_time} to #{end_time}"
  end

  def perform(product_id)
    pair = Product.find(product_id)
    first_image_download = open(pair.first_image)
    second_image_download = open(pair.last_image)
    first_image_location = "./public/uploads/#{first_image_download.base_uri.to_s.split('/')[-1]}"
    second_image_location = "./public/uploads/#{second_image_download.base_uri.to_s.split('/')[-1]}"
    IO.copy_stream(first_image_download, first_image_location)
    IO.copy_stream(second_image_download, second_image_location)

    image1_dir = File.join(*%W[#{Rails.root} public uploads #{first_image_download.base_uri.to_s.split('/')[-1]}])
    image2_dir = File.join(*%W[#{Rails.root} public uploads #{second_image_download.base_uri.to_s.split('/')[-1]}])

    images = [
      image1_dir,
      image2_dir
    ]

    processed_image = MiniMagick::Tool::Montage.new do |image|
      image.geometry "x700+0+0"
      image.tile "#{images.size}x1"
      images.each {|i| image << i}
      image << "/tmp/output_#{product_id}.jpg"
    end
    background_dir = File.join(*%W[#{Rails.root} app assets images generic_logo.png])
    watermark = MiniMagick::Image.open(background_dir)
    image = MiniMagick::Image.open("/tmp/output_#{product_id}.jpg")
    watermarked_image = image.composite(watermark) do |c|
      c.compose "Over"    # OverCompositeOp
      c.geometry "+680+650" # copy second_image onto first_image from (680 horizontally, 650 vertically)
    end
    watermarked_image.write "/tmp/final_#{product_id}.jpg"
    remote_image = Cloudinary::Uploader.upload("/tmp/final_#{product_id}.jpg")
    remote_url = remote_image["secure_url"]
    pair.update(montage_image: remote_url)
  end
end
