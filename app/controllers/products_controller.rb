class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  def index
    @products = Product.all
    respond_to do |format|
      format.html
      format.csv { send_data @products.to_csv(['id', 'first_image', 'last_image', 'montage_image']), filename: "product_images-#{DateTime.now.strftime('%Y-%m-%d-%H-%M-%S')}.csv" }
    end
  end

  # GET /products/1
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to @product, notice: 'Product was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /products/1
  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Product was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /products/1
  def destroy
    @product.destroy
    redirect_to products_url, notice: 'Product was successfully destroyed.'
  end

  def montage
    MontageWorker.perform_async(Time.now, Time.now + 1)
    render plain: "request to perform montage added to the queue"
  end

  def upload
    begin
      result = Product.import(params[:file])
      redirect_to products_url, warning: "#{result[0]} #{"image".pluralize(result[0])} imported. #{result[1].flatten.uniq.join(", ")}"
    rescue
      redirect_to products_url, danger: "Invalid or Missing CSV file."
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:first_image, :last_image, :montage_image)
    end
end
