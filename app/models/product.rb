class Product < ApplicationRecord
  validates :first_image, :last_image,
  :presence => true,
  :uniqueness => true,
  :format => {
    :with => URI::regexp(%w(http https)),
    :message => 'must be a valid URL'
  }
  validates :first_image, :last_image, :format => {
    :with => /\.(png|jpg|jpeg)\Z/i,
    :message => 'must be an image'
  }
  def self.import(file)
    counter = 0
    message = []
    CSV.foreach(file.path) do |row|
      product = Product.assign_from_row(row)
      if product.new_record? & product.save
        MontageWorker.perform_async(product.id)
        counter += 1
      else
        message << product.errors.full_messages
      end
    end
    return counter, message
  end

  def self.assign_from_row(row)
    product = Product.where(first_image: row[0]).where(last_image: row[1]).first_or_initialize
    product
  end

  def self.to_csv(headers = column_names, options = {})
    CSV.generate(headers: true) do |csv|
      csv << headers

      all.each do |product|
        csv << headers.map { |attr| product.send(attr)}
      end
    end
  end

  def montage_image
    super || "N/A"
  end
end
