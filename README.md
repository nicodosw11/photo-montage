* This test was about creating a tool using Rails with which I would be able to upload a CSV file like the example below:

| <!-- -->    | <!-- -->    |
|-------------|-------------|
| https://bec2df9eb90bb6604cfc-660d71a7a33bc04488a7427f5fddcedf.ssl.cf3.rackcdn.com/uploads/product_image/photo/582dae13ec60461353000914/xlarge_IMG_7609.jpg | https://bec2df9eb90bb6604cfc-660d71a7a33bc04488a7427f5fddcedf.ssl.cf3.rackcdn.com/uploads/product_image/photo/582dae15ec60461353000916/xlarge_IMG_7600.jpg |

For each line in the CSV there are two photo URLs, the app should create a delayed job for each line that would create a montage of the two photos. It would also add the same logo as a watermark in the bottom right.

There should be a page available where I can export a csv file with the original URLs followed by the new montage url.

It should be possible for me to launch the app myself on heroku and be able to upload a file with thousands of lines, and spin up dozens of workers to speed through them.

I was allowed to use Sinatra or something else lightweight rather than Rails, as long as its Ruby based, any gems I want to use was up to me.

# Photo Montage Tool

*Example of montage*

![alt desc](https://res.cloudinary.com/dkqgs3vrw/image/upload/v1558301480/t9mqany4dtqzuxq1l0tg.jpg)

*Example of csv after export*

| ID  | First Image | Last Image | Montage Image |
| ------------- | ------------- | ------------- | ------------- |
| 1  | https://bec2df9eb90bb6604cfc-660d71a7a33bc04488a7427f5fddcedf.ssl.cf3.rackcdn.com/uploads/product_image/photo/582dae13ec60461353000914/xlarge_IMG_7609.jpg  | https://bec2df9eb90bb6604cfc-660d71a7a33bc04488a7427f5fddcedf.ssl.cf3.rackcdn.com/uploads/product_image/photo/582dae15ec60461353000916/xlarge_IMG_7600.jpg  | https://res.cloudinary.com/dkqgs3vrw/image/upload/v1558301480/t9mqany4dtqzuxq1l0tg.jpg  |
| 2  | original image 1  | original image 2  | montage image  |

**I used Cloudinary to store the pictures**

### Improvements TODO

`Refactoring the worker method`: too much code in the montage method. Should be broken into pieces in line with the Single Responsibility Principle.

`CSV upload should update the montage image`: montage_image attribute is not updated if url is provided (or changed) in the excel import file.

`Downloading two original images into Rails app file system can be avoided`: I had to add a couple more steps because of [ImageMagick security policy] regarding https.

[ImageMagick security policy]: https://imagemagick.org/script/security-policy.php

