class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :first_image
      t.string :last_image
      t.string :montage_image

      t.timestamps
    end
  end
end
